$(function() {

var bannerSlider =  $('.banner-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    fade: true,
    pauseOnHover: false,
    arrows: false,
});

$('.banner-slider__prev').click(function(){
  $(bannerSlider).slick("slickPrev");
});
$('.banner-slider__next').click(function(){
  $(bannerSlider).slick("slickNext");
});


$('.partners-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
      }
    },
  ]
});


// BEGIN of script for header submenu
    $(".navbar-toggle").on("click", function () {
        $(this).toggleClass("active");
    });

// Поиск в шапке
$('.btn-search').click(function() {
  $(this).toggleClass('active');
  $('.header-search').fadeToggle();
})


// Выпадающее меню
if($(window).width() > 768){

	$('.dropdown').hover(function() {
		$(this).children('a').stop(true,true).toggleClass('active');
		$(this).children(".dropdown-menu").stop(true,true).fadeToggle();
	})

	$('.dropdown-menu > li').hover(function() {
	    $(this).children('ul').toggleClass('active');
	})


}


if($(window).width() < 768){

	$('.navbar-toggle').click(function() {
		$('.navbar-collapse').toggleClass('in');
		$('.navbar-nav').toggleClass('active');
	})

  $('.mobile-toggler').click(function() {
    $(this).toggleClass('active');
    $(this).next("ul").stop(true,true).fadeToggle();
  })

}






// Аккордеон
$('.faq-header').click(function(){

if(!($(this).next().hasClass('active'))){
  $('.faq-body').slideUp().removeClass('active');
  $(this).next().slideDown().addClass('active');
}else{
  $('.faq-body').slideUp().removeClass('active');
};

if(!($(this).parent('.faq-item').hasClass('active'))){
  $('.faq-item').removeClass("active");
  $(this).parent('.faq-item').addClass("active");
}else{
  $(this).parent('.faq-item').removeClass("active");

};

});



// FansyBox 2
 $('.fancybox').fancybox({
  beforeShow : function(){
   this.title =  this.title + " - " + $(this.element).data("caption");
  }
 });


// Скрипт скролла по ID
  $("a.scroll").on("click", function(event) {
      //отменяем стандартную обработку нажатия по ссылке
      event.preventDefault();

      //забираем идентификатор бока с атрибута href
      var id = $(this).attr('href'),

          //узнаем высоту от начала страницы до блока на который ссылается якорь
          top = $(id).offset().top;

      //анимируем переход на расстояние - top за 1500 мс
      $('body,html').animate({ scrollTop: top }, 1000);
  });


// Закрыть модальное окно
$('.modal-box__close').click(function() {
  $(this).parent(".modal-box").fadeOut();
})


//  Скролл вверх
$('.back-top').on('click', function(e) {
    e.preventDefault();
    $('html,body').animate({
        scrollTop: 0
    }, 700);
});


//Begin of GoogleMaps
  var myCenter=new google.maps.LatLng(56.041942, 92.843931);
  var marker;
  function initialize()
  {
    var mapProp = {
    center:myCenter,
    zoom:16,
    disableDefaultUI:true,
    zoomControl: true,
    scrollwheel: false,
    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000"},{"lightness":80}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000"},{"lightness":70},{"weight":1.2}]}],
    zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
    },
    mapTypeId:google.maps.MapTypeId.ROADMAP
    };
      var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
      var marker=new google.maps.Marker({
      position:myCenter,
      icon: "img/marker.png"
      // icon: "../img/marker.png" на  хосте может так рабоать     
      });
    marker.setMap(map);
    var content = document.createElement('div');
    content.innerHTML = '<strong class="maps-caption">Красноярский завод строп</strong>';
    var infowindow = new google.maps.InfoWindow({
     content: content
    });
      google.maps.event.addListener(marker,'click',function() {
        infowindow.open(map, marker);
        map.setCenter(marker.getPosition());
      });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
//End of GoogleMaps









})